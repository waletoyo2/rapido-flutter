import 'package:flutter/material.dart';
import 'package:rapido/rapido.dart';


import 'package:flutter_applovin_max/flutter_applovin_max.dart';

/// A floating button that invokes a form to add a new
/// document to a DocumentList. Typically used in Scaffolds.
class AddDocumentFloatingActionButton extends StatelessWidget {
  /// The DocumentList on which the button and forms that it invokes act.
  final DocumentList documentList;

  /// Optional string to describe the add action.
  /// If supplied, the fab will use an extended fab, with the label.
  final String addActionLabel;

  /// A BoxDecoration for automatically generated forms
  final BoxDecoration formDecoration;

  /// A BoxDecoration for each field in a DocumentForm
  final BoxDecoration formFieldDecoration;

  AddDocumentFloatingActionButton(this.documentList,
      {this.addActionLabel, this.formDecoration, this.formFieldDecoration});




      //add applovin

@override
  void initState() {
    super.initState();
    FlutterApplovinMax.init("b5a81f3825e599c7");
  }

  listener(AppLovinAdListener event) {
    print(event);
    if (event == AppLovinAdListener.onUserRewarded) {
      print('👍get reward');
    }
  }

  bool isRewardedVideoAvailable = false;

  _showRewarded() async {
    isRewardedVideoAvailable = await FlutterApplovinMax.isLoaded(listener);
    if (isRewardedVideoAvailable) {
      FlutterApplovinMax.showRewardVideo(
          (AppLovinAdListener event) => listener(event));
    }
  }




  @override
  Widget build(BuildContext context) {
    if (addActionLabel == null) {
      return FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
           _showRewarded();
          Navigator.push(
            context,
            MaterialPageRoute(builder: (BuildContext context) {
              return DocumentForm(
                documentList,
                decoration: formDecoration,
                fieldDecoration: formFieldDecoration,
              );
            }),
          );
        },
      );
    } else {
      return FloatingActionButton.extended(
        icon: Icon(Icons.add),
        onPressed: () {
           _showRewarded();
          Navigator.push(
            context,
            MaterialPageRoute(builder: (BuildContext context) {
              return DocumentForm(
                documentList,
                decoration: formDecoration,
                fieldDecoration: formFieldDecoration,
              );
            }),
          );
        },
        label: Text(addActionLabel),
      );
    }
  }
}
